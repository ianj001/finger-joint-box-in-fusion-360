# README #

The scripts in this repository are run from within Fusion360. They import the proprietary API.

### Making Finger Jointed Boxes in Fusion 360 CAD/CAM ###

* Input box dimensions for the length, width, height,thickness etc and get a model of the pieces ready to produce a CNC finger jointed box
* Version **0.2.0 beta**
* License **MIT**
* Author **ianj001**


### How do I get set up? ###

* Download the script or clone this repository
* Watch the video to see how to set this up in Fusion 360 https://youtu.be/V8g4HVqkGXw
* You must have Fusion 360 for this to work
* All dimensions are in millimeters. - You will have to make conversions before you run the script if you want to use other units.


### Contribution guidelines ###

* Feel free to check out the script and make improvements and submit a pull request

### Contact Information ###
ianj001 on V1Engineering forums


